from django.contrib import admin
from tasks.models import Task, Detail


@admin.register(Task)
class TaskAdmin(admin.ModelAdmin):
    pass

@admin.register(Detail)
class DetailAdmin(admin.ModelAdmin):
    pass