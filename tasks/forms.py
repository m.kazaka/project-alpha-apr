from django import forms
from tasks.models import Task, Detail


class TaskForm(forms.ModelForm):
    class Meta:
        model = Task
        fields = (
            "name",
            "start_date",
            "due_date",
            "project",
            "assignee",
        )

class DetailForm(forms.ModelForm):
    class Meta:
        model = Detail
        fields = (
            "comment",
            "is_completed",
        )
