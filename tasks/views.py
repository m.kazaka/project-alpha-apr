from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required
from tasks.forms import TaskForm, DetailForm
from tasks.models import Task


@login_required
def create_task(request):
    if request.method == "POST":
        form = TaskForm(request.POST)
        if form.is_valid():
            form = form.save()
            return redirect("list_projects")
    else:
        form = TaskForm

    context = {
        "form": form,
    }
    return render(request, "tasks/create.html", context)


@login_required
def show_my_tasks(request):
    mine = Task.objects.filter(assignee=request.user)
    context = {"mine": mine}
    return render(request, "tasks/mine.html", context)


@login_required
def edit_tasks(request):
    if request.method == "POST":
        form = DetailForm(request.POST)
        if form.is_valid():
            form = form.save()
            return redirect("show_my_tasks")
    else:
        form = DetailForm

    context = {
        "form": form,
    }
    return render(request, "tasks/mine.html", context)
